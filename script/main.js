$( document ).ready(function() {
    $('.form-register').validate({
        wrapper: 'div',
        errorLabelContainer: "#messageBox",
        errorPlacement: function(label, element) {
            label.addClass('registration-error');
            label.insertAfter(element.parent());
        },
        submitHandler: submit,
    });
    $(".btn-submit").click(function() {
        $(".form").valid()
    });
    $(".register-switch-text").click(function() {
        switchForm($(".form").hasClass("form-register"))
    });

    // Handler for .ready() called.
});

function switchForm(isLogin) {
    $(".form").validate().resetForm();
    if (isLogin) {
        $(".form").removeClass("form-register").addClass("form-login");
        $(".registration-card-title").text("Login");
        $("#password").attr("placeholder", "Password");
        $(".btn-submit").text("Login");
        $(".register-switch-text").text("Don't have an account? Register");
        $("#type").val("1");
    } else {
        $(".form").removeClass("form-login").addClass("form-register");
        $(".registration-card-title").text("Create Account");
        $("#password").attr("placeholder", "Create password");
        $(".btn-submit").text("Create Account");
        $(".register-switch-text").text("Have an account? Log In");
        $("#type").val("0");
    }
}

function submit() {
    // Constructing phone field from +7 and 999
    $("#phone").val(($("#phone_raw_select").val()+($("#phone_raw_text").val())));

    let data = $(".form").serialize();

    $.ajax({
        type: 'POST',
        url: 'form.php',
        data: data,
        beforeSend: function() {
            $("#error").fadeOut();
            $(".btn-submit").html('<span class="glyphicon glyphicon-transfer"></span>   Sending...');
        },
        success :  function(data) {
            try {
                let serialized = JSON.parse(data);
                showProfile(serialized.name, serialized.phone);
            } catch (e) {
                $(".btn-submit").text(data);
                $(".btn-submit").addClass("btn-danger");
            }
        }
    })
}

function showProfile(name, phone) {
    $(".profile-card-name").text(name);
    $(".profile-card-phone").text("+"+phone);
    $(".registration-card-body").fadeOut();
    $(".profile-card-body").fadeIn();
}