<?php
/**
 * Created by PhpStorm.
 * User: valeria
 * Date: 16/03/2019
 * Time: 15:37
 */
require_once 'db/connect.php';

if ($_POST) {
    $type = mysqli_real_escape_string($mysqli, $_POST["type"]);
    $email = mysqli_real_escape_string($mysqli, $_POST["mail"]);
    $password_raw = mysqli_real_escape_string($mysqli, $_POST["password"]);
    $password = password_hash($password_raw, PASSWORD_BCRYPT, array('cost'=>10));

    if ($type == 0) {
//        Registration
        $name = mysqli_real_escape_string($mysqli, $_POST["name"]);
        $phone = mysqli_real_escape_string($mysqli, $_POST["phone"]);
        if (strlen($phone) != 11 && strlen($phone) != 12) {
            echo "Invalid phone number";
            die();
        }
        if (strlen($password_raw) < 6) {
            echo "Too short password";
            die();
        }
        try {
            $stmt = $mysqli->prepare("SELECT name FROM users WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt->bind_result($result);

            if ($stmt->fetch()) {
                echo "User is already registered";
            } else {
                $stmt = $mysqli->prepare("INSERT INTO users(name, email, password, phone) VALUES(?, ?, ?, ?)");
                $stmt->bind_param("ssss", $name, $email, $password, $phone);

                if ($stmt->execute()) {
                    echo "Registration successful";
                } else {
                    echo "Failed to execute query";
                }
            }

        } catch(mysqli_sql_exception $e) {
            echo $e->getMessage();
        }

    } else if ($type == 1) {
//        Login
        try {
            $stmt = $mysqli->prepare("SELECT password, name, phone FROM users WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt->bind_result($password_hash, $name, $phone);

            if ($stmt->fetch()) {
                if (password_verify($password_raw, $password_hash)) {
                    echo json_encode(array('name' => $name, 'phone' => $phone));
                } else {
                    echo "Invalid username or password";
                }
            } else {
//                No user
                echo "Invalid username or password";
            }

        } catch(mysqli_sql_exception $e) {
            echo $e->getMessage();
        }


    } else {
//        Something else
        echo "FAILED TO GET TYPE";
    }
}

?>